#ifndef CIRCBUF_H
#define CIRCBUF_H

#include <Arduino.h>

#define CIRCBUF_LEN 16

// FIFO Buffer with a limited number of element
template<typename T>
class CircularBuffer
{
private:
	T _buffer[CIRCBUF_LEN];
	byte _readIdx;
	byte _writeIdx;
	byte _nElem;
	bool _mutex;

	void takeMutex(void) {
		while(_mutex) {
			delay(1);
		}
		_mutex = true;
	}

	void releaseMutex(void) {
		_mutex = false;
	}

public:
	CircularBuffer()
		: _readIdx(0), _writeIdx(0), _nElem(0), _mutex(false)
	{}
	
	bool put(const T& element) {
		takeMutex();
		
		if(_nElem<=CIRCBUF_LEN) {
			_buffer[_writeIdx] = element;
			_writeIdx = (_writeIdx+1) % CIRCBUF_LEN;
			_nElem++;

			releaseMutex();
			return true;
		}
		
		releaseMutex();
		return false;
	}
	
	bool get(T& element) {
		takeMutex();
		
		if(_nElem>0) {
			element = _buffer[_readIdx];
			_readIdx = (_readIdx+1) % CIRCBUF_LEN;
			_nElem--;
			
			releaseMutex();
			return true;
		}
		
		releaseMutex();
		return false;
	}
};

#endif // CIRCBUF_H
