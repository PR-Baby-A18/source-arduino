#include <PinChangeInterrupt.h>
#include <PinChangeInterruptBoards.h>
#include <PinChangeInterruptSettings.h>
#include <PinChangeInterruptPins.h>

#include <SPI.h>
#include <MFRC522.h>
#include "com.h"

#define RST_PIN    9
#define SS_PIN    10

#define DELAY_BETWEEN_RFID_MS 6000

static const byte buttonPins[] = {A0, A1, A2, A3, A4};
static const byte goalPin      = 2;
static const byte RFID_RST_PIN = 9;
static const byte RFID_SS_PIN  = 10;

volatile static byte currentStates;
volatile static byte previousStates;
static uint32_t currentRFID;
static uint32_t previousRFID;
static uint32_t lastRFIDTime;

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance
Com com;

ISR (PCINT1_vect) // handle pin change interrupt for A0 to D5
{
	updateButtons();
}

void updateButtons(void) {
	// The interrupt is called whenever one of the 6 button is pushed or release
	// This is to determine which one of them have changed
	// and if comes from a press or release

	byte i = 0;
	currentStates = 0;
	
	while(i<sizeof(buttonPins)) {
		currentStates |= !digitalRead(buttonPins[i]) << i;

		// XOR to reveal differences and checking that it is indeed up
		if(bitRead(currentStates ^ previousStates, i)==1 && bitRead(currentStates, i)==1) {
			com.sendButton(i);
			i = sizeof(buttonPins);
		} else {
			i++;
		}
	}

	previousStates = currentStates;
}

void checkGoal(void) {
	// Later it will measure time difference to check if a ball got in
	// the way of one or another goal and send that goal

	if(digitalRead(goalPin)==1)
	{
		com.sendGoal();
	}
}

void pciSetup(byte pin)
{
	*digitalPinToPCMSK(pin) |= bit (digitalPinToPCMSKbit(pin));  // enable pin
	PCIFR  |= bit (digitalPinToPCICRbit(pin)); // clear any outstanding interrupt
	PCICR  |= bit (digitalPinToPCICRbit(pin)); // enable interrupt for the group
}

void setup() {
	Serial.begin(9600);  // Initialize serial communications with the PC
	
	for(byte i=0; i<sizeof(buttonPins); i++) {
		pinMode(buttonPins[i], INPUT_PULLUP);
		pciSetup(buttonPins[i]);
	}
	
	pinMode(goalPin, INPUT_PULLUP);
	attachInterrupt(digitalPinToInterrupt(goalPin), checkGoal, CHANGE);
	
	SPI.begin();         // Init SPI bus
	mfrc522.PCD_Init();  // Init MFRC522 card
}

void loop() {
	// Look for new cards, and select one if present
	if (mfrc522.PICC_IsNewCardPresent() && mfrc522.PICC_ReadCardSerial()) {
		currentRFID = 0;
		for (byte i = 0; i < mfrc522.uid.size; i++) {
			currentRFID |= ((uint32_t)mfrc522.uid.uidByte[i]) << 8*(mfrc522.uid.size-i-1);
		}

		if((currentRFID!=previousRFID) || ((millis()-lastRFIDTime)>DELAY_BETWEEN_RFID_MS)) {
			com.sendRFID(currentRFID);
			previousRFID = currentRFID;
			lastRFIDTime = millis();
		}
	}

	if(com.emitMessage()) {
		delay(20);
	} else {
		delay(100);
	}
}
