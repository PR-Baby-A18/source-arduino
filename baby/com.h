#ifndef COM_H
#define COM_H

#include <stdint.h>
#include "circbuf.h"

class Com
{
	typedef struct {
		enum class Type {
			None   = 0,
			RFID,
			Butn,
			Goal
		};
		
		Type type;
		uint32_t content;
	} Message;
private:
	CircularBuffer<Message> _buffer;
	bool addMessage(Message::Type type, uint32_t content = 0);

public:
	bool sendRFID(uint32_t rfid);
	bool sendButton(uint8_t buttonID);
	bool sendGoal();
	
	bool emitMessage();
};

#endif // COM_H
