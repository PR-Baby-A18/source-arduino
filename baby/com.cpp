#include "com.h"
#include <Arduino.h>

bool Com::addMessage(Message::Type type, uint32_t content) {
  Message msg = {.type = type, .content = content};
  return _buffer.put(msg);
}

bool Com::sendRFID(uint32_t rfid) {
  return addMessage(Message::Type::RFID, rfid);
}

bool Com::sendButton(uint8_t buttonID) {
  return addMessage(Message::Type::Butn, buttonID);
}

bool Com::sendGoal() {
  return addMessage(Message::Type::Goal);
}

bool Com::emitMessage() {
  Message msgToSend = {.type = Message::Type::None, .content = 0};

  // Warning &?
  if (_buffer.get(msgToSend)) {
  	switch(msgToSend.type)
  	{
  		case Message::Type::RFID: Serial.print("rfid"); break;
  		case Message::Type::Butn: Serial.print("butn"); break;
  		case Message::Type::Goal: Serial.print("goal"); break;
  	}
  	
    Serial.print(":");
    Serial.println(msgToSend.content, HEX);
    return true;
  }

  return false;
}


